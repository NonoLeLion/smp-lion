# SMP Lion 🦁

## Règles

 - Ne pas faire volontairement crash le serveur
 - Pas de client / mods de hack (mini HUD, Tweakeroo, Replay mod, **ne sont PAS considéré comme du cheat**)

 *(Tout ce qui n'est pas dans les règles est autorisé)*

## Ressources

 - [CurseForge mod pack](https://gitlab.com/NonoLeLion/smp-lion/-/raw/main/curseforgepack/SMP%20Lion-1.0.0.zip?inline=false)
 - [Resourcepack](https://gitlab.com/NonoLeLion/smp-lion/-/raw/main/resourcepacks/SMP%20Lion%20Resources.zip?inline=false)

*Rien de ceci n'est obligatoire, mais fortement recommander* 🙂

## Infos

Pour ceux qui ne veulent pas lancer avec curseforge (genre les FDP qui utilisent LunarClient à tout hasard), vous pouvez télécharger le mod **Simple Voice Chat**
juste [ici](https://www.curseforge.com/minecraft/mc-mods/simple-voice-chat)<br>
*PS: pour LunarClient faut prendre pour le modloader Forge, pas pour Fabric*
